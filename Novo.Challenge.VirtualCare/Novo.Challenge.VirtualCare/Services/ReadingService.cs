﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Novo.Challenge.VirtualCare.Database;
using Novo.Challenge.VirtualCare.Database.Documents;
using Novo.Challenge.VirtualCare.Mappings;
using Novo.Challenge.VirtualCare.Models;

namespace Novo.Challenge.VirtualCare.Services
{
    public interface IReadingService
    {
        Task<ReadingSummaryDto> GetReadingsForPatient(string id);

        Task CreateReadings(List<ReadingDto> reading);
    }

    public class ReadingService : IReadingService
    {
        private readonly IReadingOperationManager m_readingDb;
        private readonly IVirtualCareMapper m_mapper;

        public ReadingService(IReadingOperationManager readingDb, IVirtualCareMapper mapper)
        {
            m_readingDb = readingDb;
            m_mapper = mapper;
        }

        public async Task<ReadingSummaryDto> GetReadingsForPatient(string id)
        {
            var readings = await m_readingDb.GetReadingsForPatientAsync(id);
            return m_mapper.Map<ReadingSummaryDto>(readings);
        }

        public async Task CreateReadings(List<ReadingDto> reading) { await m_readingDb.CreateReadingsAsync(m_mapper.Map<List<Reading>>(reading)); }
    }
}