﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Novo.Challenge.VirtualCare.Database;
using Novo.Challenge.VirtualCare.Database.Documents;
using Novo.Challenge.VirtualCare.Mappings;
using Novo.Challenge.VirtualCare.Models;

namespace Novo.Challenge.VirtualCare.Services
{
    public interface IPatientService
    {
        Task<List<PatientDto>> GetPatientsAsync();

        Task<PatientDto> GetPatientAsync(string id);

        Task<PatientDto> CreatePatientAsync(PatientDto patient);

        Task<bool> UpdatePatientAsync(PatientDto patient);

        Task<bool> DeletePatientAsync(string id);
    }

    public class PatientService : IPatientService
    {
        private readonly IPatientDbOperationManager m_patientDb;
        private readonly IVirtualCareMapper m_mapper;

        public PatientService(IPatientDbOperationManager patientDb, IVirtualCareMapper mapper)
        {
            m_patientDb = patientDb;
            m_mapper = mapper;
        }

        public async Task<List<PatientDto>> GetPatientsAsync()
        {
            var patients = await m_patientDb.GetPatientsAsync();
            return m_mapper.Map<List<PatientDto>>(patients);
        }

        public async Task<PatientDto> GetPatientAsync(string id)
        {
            var patient = await m_patientDb.GetPatientAsync(id);
            return m_mapper.Map<PatientDto>(patient);
        }

        public async Task<PatientDto> CreatePatientAsync(PatientDto patient)
        {
            patient.Id = null;
            var newPatient = m_mapper.Map<Patient>(patient);
            await m_patientDb.CreatePatientAsync(newPatient);
            patient.Id = newPatient.Id;
            return patient;
        }

        public async Task<bool> UpdatePatientAsync(PatientDto patient) { return await m_patientDb.UpdatePatientAsync(m_mapper.Map<Patient>(patient)); }

        public async Task<bool> DeletePatientAsync(string id) { return await m_patientDb.RemovePatientAsync(id); }
    }
}