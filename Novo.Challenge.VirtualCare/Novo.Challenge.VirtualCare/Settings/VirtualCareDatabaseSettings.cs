﻿namespace Novo.Challenge.VirtualCare.Settings
{
    public class VirtualCareDatabaseSettings
    {
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;
    }
}