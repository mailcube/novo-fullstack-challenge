﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Bogus.Healthcare;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Novo.Challenge.VirtualCare.Events;
using Novo.Challenge.VirtualCare.Events.Messages;
using Novo.Challenge.VirtualCare.Models;
using Novo.Challenge.VirtualCare.Services;

namespace Novo.Challenge.VirtualCare.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReadingsController : ControllerBase
    {
        private readonly ILogger<ReadingsController> m_logger;
        private readonly IReadingService m_service;
        private readonly IVirtualCarePublisher m_publisher;
        private readonly Faker<ReadingDto> m_readingFaker;

        public ReadingsController(IReadingService service, IVirtualCarePublisher publisher, ILogger<ReadingsController> logger)
        {
            m_logger = logger;
            m_service = service;
            m_publisher = publisher;

            m_readingFaker = new Faker<ReadingDto>()
                             .RuleFor(r => r.Timestamp, f => DateTime.UtcNow)
                             .RuleFor(r => r.OxygenSaturation, f => f.Random.Double(95, 100))
                             .RuleFor(r => r.TemperatureFarenheit, f => f.Random.Double(97, 99))
                             .RuleFor(r => r.HeartRatePerMinute, f => f.Random.Int(60, 100));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ReadingSummaryDto>> GetReading(string id) { return await m_service.GetReadingsForPatient(id); }

        [HttpPost]
        [ProducesResponseType(typeof(string), StatusCodes.Status201Created)]
        public async Task<ActionResult> PostReading([FromBody] List<ReadingDto> reading)
        {
            await m_service.CreateReadings(reading);
            foreach (var msg in reading.Select(r => new ReadingMessage(r)))
            {
                m_publisher.Publish(msg);
            }

            return CreatedAtAction(nameof(GetReading), null);
        }

        [HttpPost("{id}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status201Created)]
        public async Task<ActionResult> PostRandomReading(string id)
        {
            var reading = m_readingFaker.Generate();
            reading.PatientId = id;

            await m_service.CreateReadings(new List<ReadingDto> { reading });
            m_publisher.Publish(new ReadingMessage(reading));

            return CreatedAtAction(nameof(GetReading), null);
        }
    }
}