﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Novo.Challenge.VirtualCare.Models;
using Novo.Challenge.VirtualCare.Services;

namespace Novo.Challenge.VirtualCare.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PatientsController : ControllerBase
    {
        private readonly ILogger<PatientsController> m_logger;
        private readonly IPatientService m_service;

        public PatientsController(ILogger<PatientsController> logger, IPatientService service)
        {
            m_logger = logger;
            m_service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PatientDto>>> Get()
        {
            return await m_service.GetPatientsAsync();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PatientDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PatientDto>> GetPatient(string id)
        {
            var patient = await m_service.GetPatientAsync(id);
            if (patient is null)
                return NotFound();

            return patient;
        }

        [HttpPost]
        [ProducesResponseType(typeof(string), StatusCodes.Status201Created)]
        public async Task<ActionResult<PatientDto>> PostPatient([FromBody] PatientDto patient)
        {
            return CreatedAtAction(nameof(GetPatient), await m_service.CreatePatientAsync(patient));
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PatientDto>> PutPatient([FromBody] PatientDto patient)
        {
            if (await m_service.UpdatePatientAsync(patient))
                return NoContent();

            return NotFound();
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(PatientDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PatientDto>> DeletePatient(string id)
        {
            if (await m_service.DeletePatientAsync(id))
                return Ok();

            return NotFound();
        }
    }
}