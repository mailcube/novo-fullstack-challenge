using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Novo.Challenge.VirtualCare.Database.Documents;
using Novo.Challenge.VirtualCare.Settings;

namespace Novo.Challenge.VirtualCare.Database
{
    /// <summary>
    /// DB Operation utility to interact with patient documents inside a MongoDB instance.
    /// </summary>
    public interface IPatientDbOperationManager
    {
        Task<List<Patient>> GetPatientsAsync();

        Task<Patient?> GetPatientAsync(string id);

        Task CreatePatientAsync(Patient patient);

        Task<bool> UpdatePatientAsync(Patient patient);

        Task<bool> RemovePatientAsync(string id);
    }

    public class PatientOperationManager : DbOperationManager<Patient>, IPatientDbOperationManager
    {
        public PatientOperationManager(IOptions<VirtualCareDatabaseSettings> settings) : base(settings) { }

        public async Task<List<Patient>> GetPatientsAsync() => await Collection.Find(_ => true).ToListAsync();

        public async Task<Patient?> GetPatientAsync(string id) => await Collection.Find(p => p.Id == id).FirstOrDefaultAsync();

        public async Task CreatePatientAsync(Patient patient) { await Collection.InsertOneAsync(patient); }

        public async Task<bool> UpdatePatientAsync(Patient patient) => (await Collection.ReplaceOneAsync(p => p.Id == patient.Id, patient)).IsAcknowledged;

        public async Task<bool> RemovePatientAsync(string id) => (await Collection.DeleteOneAsync(p => p.Id == id)).IsAcknowledged;
    }
}