using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Novo.Challenge.VirtualCare.Database.Documents;
using Novo.Challenge.VirtualCare.Settings;

namespace Novo.Challenge.VirtualCare.Database
{
    /// <summary>
    /// DB Operation utility to interact with reading documents inside a MongoDB instance.
    /// </summary>
    public interface IReadingOperationManager
    {
        Task<List<Reading>> GetReadingsForPatientAsync(string id);

        Task CreateReadingAsync(Reading reading);

        Task CreateReadingsAsync(List<Reading> reading);
    }

    public class ReadingOperationManager : DbOperationManager<Reading>, IReadingOperationManager
    {
        public ReadingOperationManager(IOptions<VirtualCareDatabaseSettings> settings) : base(settings) { }

        public async Task<List<Reading>> GetReadingsForPatientAsync(string id) => await Collection.Find(r => r.PatientId == id).ToListAsync();

        public async Task CreateReadingAsync(Reading reading) { await Collection.InsertOneAsync(reading); }

        public async Task CreateReadingsAsync(List<Reading> readings) { await Collection.InsertManyAsync(readings); }
    }
}