﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Novo.Challenge.VirtualCare.Models;

namespace Novo.Challenge.VirtualCare.Database.Documents
{
    public class Reading : IReading
    {
        public DateTime Timestamp { get; set; }
        public double HeartRatePerMinute { get; set; }
        public double TemperatureFarenheit { get; set; }
        public double OxygenSaturation { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string PatientId { get; set; }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }
}