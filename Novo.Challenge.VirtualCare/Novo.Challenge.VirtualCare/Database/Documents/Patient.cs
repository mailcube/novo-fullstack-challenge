﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Novo.Challenge.VirtualCare.Models;

namespace Novo.Challenge.VirtualCare.Database.Documents
{
    public class Patient : IPatient
    {
        public string Email { get; set; } = null!;
        public string Firstname { get; set; } = null!;
        public string Lastname { get; set; } = null!;
        public ushort Age { get; set; }
        public string Gender { get; set; }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }
}