using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Novo.Challenge.VirtualCare.Settings;

namespace Novo.Challenge.VirtualCare.Database
{
    /// <summary>
    /// Base class for operations required on a MongoDB instance.
    /// Inheritors are provided a collection for their defined document type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DbOperationManager<T>
    {
        protected readonly IMongoCollection<T> Collection;

        public DbOperationManager(IOptions<VirtualCareDatabaseSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            var database = client.GetDatabase(settings.Value.DatabaseName);

            Collection = database.GetCollection<T>(typeof(T).Name);
        }
    }
}