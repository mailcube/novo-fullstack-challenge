﻿namespace Novo.Challenge.VirtualCare.Models
{
    public class ReadingSummaryDto
    {
        public string PatientId { get; set; }

        public double HeartRateAverage { get; set; }
        public double HeartRateMax { get; set; }
        public double HeartRateMin { get; set; }

        public double OxygenSaturationAverage { get; set; }
        public double OxygenSaturationMax { get; set; }
        public double OxygenSaturationMin { get; set; }

        public double TemperatureFarenheitAverage { get; set; }
        public double TemperatureFarenheitMax { get; set; }
        public double TemperatureFarenheitMin { get; set; }
    }
}