using System;

namespace Novo.Challenge.VirtualCare.Models
{
    public interface IReading
    {
        DateTime Timestamp { get; set; }
        double HeartRatePerMinute { get; set; }
        double TemperatureFarenheit { get; set; }
        double OxygenSaturation { get; set; }
        string PatientId { get; set; }
    }
}