namespace Novo.Challenge.VirtualCare.Models
{
    public class PatientDto : IPatient
    {
        public string Email { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public ushort Age { get; set; }
        
        public string Gender { get; set; }
        public string Id { get; set; }
    }
}