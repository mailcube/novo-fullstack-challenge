﻿namespace Novo.Challenge.VirtualCare.Models
{
    public interface IPatient
    {
        string Email { get; set; }
        string Firstname { get; set; }
        string Lastname { get; set; }
        ushort Age { get; set; }
        string Gender { get; set; }
        public string Id { get; set; }
    }
}