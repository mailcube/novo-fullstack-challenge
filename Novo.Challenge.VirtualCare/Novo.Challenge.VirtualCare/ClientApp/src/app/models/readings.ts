export class Reading {
  timestamp: Date
  heartRatePerMinute: number
  temperatureFarenheit: number
  oxygenSaturation: number
  patientId: string
}

export class ReadingSummary {
  patientId: string
  heartRateAverage: number
  heartRateMax: number
  heartRateMin: number
  oxygenSaturationAverage: number
  oxygenSaturationMax: number
  oxygenSaturationMin: number
  temperatureFarenheitAverage: number
  temperatureFarenheitMax: number
  temperatureFarenheitMin: number
}
