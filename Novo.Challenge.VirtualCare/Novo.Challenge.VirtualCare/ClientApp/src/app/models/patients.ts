export class Patient {
  id: string
  firstName: string
  lastName: string
  email: string
  age: number
  gender: string
}
