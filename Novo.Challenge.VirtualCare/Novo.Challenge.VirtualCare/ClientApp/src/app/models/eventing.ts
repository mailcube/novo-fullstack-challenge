﻿export class ReadingReceived{
  patientId: string
  heartRateAverage: number
  heartRateMax: number
  heartRateMin: number
  oxygenSaturationAverage: number
  oxygenSaturationMax: number
  oxygenSaturationMin: number
  temperatureFarenheitAverage: number
  temperatureFarenheitMax: number
  temperatureFarenheitMin: number
}
