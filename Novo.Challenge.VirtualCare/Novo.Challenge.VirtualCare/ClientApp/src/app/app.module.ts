import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {MatSliderModule} from "@angular/material/slider";

import {AppComponent} from './app.component';
import {NavMenuComponent} from './components/nav-menu/nav-menu.component';
import {HomeComponent} from './components/home/home.component';
import {PatientComponent} from './components/patient/patient.component';
import {ShowPatientsComponent} from './components/dashboard/show-patients/show-patients.component';
import {
  AddEditPatientsDialogComponent
} from './components/dashboard/add-edit-patients/add-edit-patients-dialog.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {ViewPatientChartsComponent} from './components/dashboard/view-patient-charts/view-patient-charts.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatMenuModule} from "@angular/material/menu";
import {MatButtonModule} from "@angular/material/button";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatTableModule} from "@angular/material/table";
import {MatDialogModule} from "@angular/material/dialog";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatSnackBarModule} from "@angular/material/snack-bar";

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    PatientComponent,
    ShowPatientsComponent,
    AddEditPatientsDialogComponent,
    DashboardComponent,
    ViewPatientChartsComponent
  ],
  entryComponents: [
    AddEditPatientsDialogComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    RouterModule.forRoot([
      // {path: 'patient/:id', component: ViewPatientSummary},
      {path: '', component: HomeComponent, pathMatch: 'full'},
      {path: 'dashboard', component: DashboardComponent, pathMatch: 'full'},
      {path: 'summary', component: PatientComponent, pathMatch: 'full'},
    ]),
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
