import { Component, OnInit } from '@angular/core';
import {PatientService} from "../../services/patient.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {AddEditPatientsDialogComponent} from "./add-edit-patients/add-edit-patients-dialog.component";
import {Patient} from "../../models/patients";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  constructor(private service:PatientService, public dialog:MatDialog, private snackBar: MatSnackBar) { }

  public addPatient() {
    const dialogRef = this.dialog.open(AddEditPatientsDialogComponent, {
      width: '30%',
      data: new Patient()
    })

    dialogRef.componentInstance.title = "Create new patient";

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.service.savePatient(result).subscribe(() =>{
          this.snackBar.open("Successfully created patient " + result.firstName + " " + result.lastName)
        },
          error => console.error("Error while saving new patient" + error.message))
      }
    })
  }
}

