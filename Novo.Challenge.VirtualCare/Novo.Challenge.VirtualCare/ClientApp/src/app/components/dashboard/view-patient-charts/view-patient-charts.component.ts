import { Component, OnInit } from '@angular/core';
import {PatientService} from "../../../services/patient.service";
import {ReadingService} from "../../../services/reading.service";

@Component({
  selector: 'app-view-charts',
  templateUrl: './view-patient-charts.component.html',
  styleUrls: ['./view-patient-charts.component.css']
})
export class ViewPatientChartsComponent implements OnInit {
  constructor(private service:ReadingService) { }

  ngOnInit() {
  }
}
