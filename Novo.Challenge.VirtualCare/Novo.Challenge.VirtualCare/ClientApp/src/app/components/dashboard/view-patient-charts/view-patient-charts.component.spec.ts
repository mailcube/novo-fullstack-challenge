import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPatientChartsComponent } from './view-patient-charts.component';

describe('ViewChartsComponent', () => {
  let component: ViewPatientChartsComponent;
  let fixture: ComponentFixture<ViewPatientChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPatientChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPatientChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
