import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPatientsDialogComponent } from './add-edit-patients-dialog.component';

describe('AddEditPatientsComponent', () => {
  let component: AddEditPatientsDialogComponent;
  let fixture: ComponentFixture<AddEditPatientsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditPatientsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPatientsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
