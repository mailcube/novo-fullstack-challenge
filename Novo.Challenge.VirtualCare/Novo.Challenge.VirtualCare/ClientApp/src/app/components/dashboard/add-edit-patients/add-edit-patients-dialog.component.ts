import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Patient} from "../../../models/patients";
import {FormBuilder, FormGroup} from "@angular/forms";

export interface Gender {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-edit-patients',
  templateUrl: './add-edit-patients-dialog.component.html',
  styleUrls: ['./add-edit-patients-dialog.component.css']
})
export class AddEditPatientsDialogComponent {

  title: string;
  genders: Gender[] = [
    {value: 'M', viewValue: 'Male'},
    {value: 'F', viewValue: 'Female'},
    {value: 'O', viewValue: 'Other'}
  ];

  constructor(public dialogRef: MatDialogRef<AddEditPatientsDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public patient: Patient) {
  }

  onCancelClick() {
    this.dialogRef.close();
  }
}

