import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Patient} from "../../../models/patients";
import {PatientService} from "../../../services/patient.service";
import {MatDialog} from "@angular/material/dialog";
import {AddEditPatientsDialogComponent} from "../add-edit-patients/add-edit-patients-dialog.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-show-patients',
  templateUrl: './show-patients.component.html',
  styleUrls: ['./show-patients.component.css']
})
export class ShowPatientsComponent implements OnInit {

  @Output() operationSuccess: EventEmitter<void> = new EventEmitter<void>();
  @Output() operationFailure: EventEmitter<void> = new EventEmitter<void>();

  public patients: Patient[];
  public displayedColumns: string[] = [
    nameof<Patient>("firstName"),
    nameof<Patient>("lastName"),
    nameof<Patient>("email"),
    nameof<Patient>("age"),
    nameof<Patient>("gender"),
    "options"
  ];

  constructor(private service: PatientService, public dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.refreshPatients();
  }

  private refreshPatients() {
    this.service.getPatients().subscribe(patients => {
      this.patients = patients;
    });
  }

  editPatient(patient: Patient) {
    const dialogRef = this.dialog.open(AddEditPatientsDialogComponent, {
      width: '30%',
      data: patient
    })

    dialogRef.componentInstance.title = "Edit patient";

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.savePatient(result).subscribe(() => {
          this.snackBar.open("Successfully modified patient " + result.firstName + " " + result.lastName)
        })
      }
    })
  }

  deletePatient(patient: Patient) {
    this.service.deletePatient(patient.id).subscribe(
      () => {
        this.operationSuccess.emit();
        this.refreshPatients();
      },
      () => {
        this.operationFailure.emit();
      });
  }
}

const nameof = <T>(name: keyof T) => name;

