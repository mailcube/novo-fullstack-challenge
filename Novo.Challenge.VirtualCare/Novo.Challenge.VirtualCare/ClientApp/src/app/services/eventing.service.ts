﻿import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {HubConnection, HubConnectionBuilder, LogLevel} from "@aspnet/signalr";
import {ReadingReceived} from "../models/eventing";

@Injectable({providedIn: 'root'})
export class EventingService {
  private READING_RECEIVED_EVENT_NAME = "ReadingReceived";
  private connection: HubConnection

  public readingReceived$: Subject<ReadingReceived> = new Subject<ReadingReceived>();

  constructor() {
    this.initialize();
    this.start();
  }

  private initialize(): void{
    this.connection = new HubConnectionBuilder()
      .withUrl("/eventhub")
      .configureLogging(LogLevel.Information)
      .build();

    this.connection.onclose(err =>{
      if(err){
        console.error("SignalR connection closed with fault: " + err.message + " Stack: " + err.stack)
      }else{
        console.log("SignalR connection closed")
      }
    });

    this.connection.on(this.READING_RECEIVED_EVENT_NAME, (reading) => this.readingReceived$.next(reading));
  }

  private start(){
    this.connection.start();
  }
}
