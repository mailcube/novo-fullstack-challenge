import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable, BehaviorSubject, throwError, of} from "rxjs";
import {Patient} from "../models/patients";
import {catchError, finalize, map} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class PatientService{

  private patients$: BehaviorSubject<Patient[]>
  private patientEndpoint: string = "api/Patients"

  constructor(private http: HttpClient) {
  }

  public getPatients():Observable<Patient[]>{
    if(!this.patients$){
      this.patients$ = new BehaviorSubject<Patient[]>([]);
      this.refresh();
    }
    return this.patients$.asObservable();
  }

  public getPatient(id: string): Observable<Patient>{
    return this.http.get<Patient>(this.patientEndpoint + "/" + id);
  }

  public savePatient(patient: Patient): Observable<any> {

    let currentPatients = (this.patients$)
      ? (this.patients$.getValue())
        ? this.patients$.getValue()
        : []
      : [];

    let response$ : Observable<any> =
      currentPatients.findIndex(p => p.id === patient.id) >= 0
        ? this.http.put(this.patientEndpoint + "/" + patient.id, patient)
        : this.http.post(this.patientEndpoint, patient);

    return response$
      .pipe(
        map(() => of(true)),
        finalize(() => this.refresh()),
        catchError((e) => throwError(e))
      )
  }

  public deletePatient(id: string): Observable<any> {
    return this.http.delete(this.patientEndpoint + "/" + id)
      .pipe(
        map(() => of(true)),
        finalize(() => this.refresh()),
        catchError((e) => throwError(e))
      )
  }


  private refresh(){
    this.http.get<Patient[]>(this.patientEndpoint).subscribe((patients: Patient[]) => {
      this.patients$.next(patients);
    });
  }
}
