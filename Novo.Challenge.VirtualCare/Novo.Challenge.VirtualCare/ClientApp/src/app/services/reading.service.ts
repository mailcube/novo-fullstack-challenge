import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable, BehaviorSubject, of, throwError} from "rxjs";
import {EventingService} from "./eventing.service";
import {ReadingReceived} from "../models/eventing";
import {filter} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class ReadingService{

  private readingEndpoint: string = "api/Readings"

  constructor(private http: HttpClient, private eventService: EventingService) {
  }

  public listenReadings(patientId:string): Observable<ReadingReceived>{
    return this.eventService.readingReceived$.pipe(filter(r => r.patientId === patientId));
  }
}
