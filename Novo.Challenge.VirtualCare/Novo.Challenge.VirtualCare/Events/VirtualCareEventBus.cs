﻿using System;
using System.Reactive.Subjects;
using Novo.Challenge.VirtualCare.Events.Messages;

namespace Novo.Challenge.VirtualCare.Events
{
    /// <summary>
    /// Publisher employed when raising event to notify components registered to the message
    /// </summary>
    public interface IVirtualCarePublisher
    {
        void Publish(Message msg);
    }

    /// <summary>
    /// Used by components in the server that needs to listen and subscribe to messages that were published through the bus
    /// </summary>
    public interface IVirtualCareListener : IObservable<Message>
    {
        public IDisposable Subscribe(Action<Message> action);
    }

    public class VirtualCareEventBus : IVirtualCareListener, IVirtualCarePublisher
    {
        private readonly Subject<Message> m_subject = new Subject<Message>();

        public IDisposable Subscribe(IObserver<Message> observer) => m_subject.SubscribeSafe(observer);

        public IDisposable Subscribe(Action<Message> action) => m_subject.Subscribe(msg => {
            try
            {
                m_subject.OnNext(msg);
            }
            catch (Exception e)
            {
                m_subject.OnError(e);
            }
        });

        public void Publish(Message msg) => m_subject.OnNext(msg);
    }
}