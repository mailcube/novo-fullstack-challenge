﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Novo.Challenge.VirtualCare.Events.Messages;
using Novo.Challenge.VirtualCare.Mappings;
using Novo.Challenge.VirtualCare.Models;
using Novo.Challenge.VirtualCare.Services;
using Novo.Challenge.VirtualCare.SignalR;

namespace Novo.Challenge.VirtualCare.Events
{
    public class EventHub : Hub<IEventHubClient>, IEventHubClient
    {
        private readonly IReadingService m_service;
        private readonly IVirtualCareMapper m_mapper;
        private readonly ILogger<EventHub> m_logger;

        private readonly IDisposable m_subscription;

        public EventHub(IReadingService service, IVirtualCareListener listener, IVirtualCareMapper mapper, ILogger<EventHub> logger)
        {
            m_service = service;
            m_mapper = mapper;
            m_logger = logger;

            m_subscription = listener.OfType<ReadingMessage>().Select(msg => Observable.FromAsync(_ => OnReadingReceived(msg))).Concat().Subscribe();
        }

        public async Task ReadingReceived(ReadingReceivedPayload payload)
        {
            m_logger.LogDebug("Received a reading payload for patient {PatientId}", payload.PatientId);
            await m_service.CreateReadings(new List<ReadingDto> { m_mapper.Map<ReadingDto>(payload) });
            await Clients.All.ReadingReceived(payload);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                m_subscription.Dispose();
            }
        }

        private async Task OnReadingReceived(ReadingMessage message)
        {
            m_logger.LogDebug("Received a reading message for patient {PatientId}", message.Reading.PatientId);
            await Clients.All.ReadingReceived(m_mapper.Map<ReadingReceivedPayload>(message.Reading));
        }
    }
}