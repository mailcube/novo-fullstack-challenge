﻿using System;

namespace Novo.Challenge.VirtualCare.Events.Messages
{
    public abstract class Message
    {
        public DateTime Timestamp { get; set; } = DateTime.UtcNow;

        public Message WithContextFrom(Message msg)
        {
            Timestamp = msg.Timestamp;
            return this;
        }
    }
}