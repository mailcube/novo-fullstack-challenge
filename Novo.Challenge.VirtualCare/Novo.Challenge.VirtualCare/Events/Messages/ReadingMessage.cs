﻿using Novo.Challenge.VirtualCare.Models;

namespace Novo.Challenge.VirtualCare.Events.Messages
{
    public class ReadingMessage : Message
    {
        public ReadingDto Reading { get; }

        public ReadingMessage(ReadingDto reading)
        {
            Reading = reading;
            Timestamp = reading.Timestamp;
        }
    }
}