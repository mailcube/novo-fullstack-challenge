﻿using System.Collections.Generic;
using System.Linq;
using Mapster;
using Novo.Challenge.VirtualCare.Database.Documents;
using Novo.Challenge.VirtualCare.Models;
using Novo.Challenge.VirtualCare.SignalR;

namespace Novo.Challenge.VirtualCare.Mappings
{
    public class MapperDefinition : TypeAdapterConfig
    {
        public MapperDefinition()
        {
            NewConfig<Patient, PatientDto>();
            NewConfig<PatientDto, Patient>();
            NewConfig<Reading, ReadingDto>();
            NewConfig<ReadingDto, Reading>();
            NewConfig<ReadingReceivedPayload, ReadingDto>();
            NewConfig<ReadingDto, ReadingReceivedPayload>();

            NewConfig<List<Reading>, ReadingSummaryDto>().ConstructUsing(readings => BuildSummary(readings));
        }

        private ReadingSummaryDto BuildSummary(List<Reading> readings)
        {
            if (!readings.Any()) return default;

            var heartrates = readings.Select(r => r.HeartRatePerMinute).ToList();
            var saturations = readings.Select(r => r.OxygenSaturation).ToList();
            var temperatures = readings.Select(r => r.TemperatureFarenheit).ToList();

            // This is very messy and likely not performant with large volumes - Requires benchmarks
            return new ReadingSummaryDto {
                HeartRateAverage = heartrates.Average(),
                HeartRateMax = heartrates.Max(),
                HeartRateMin = heartrates.Min(),
                OxygenSaturationAverage = saturations.Average(),
                OxygenSaturationMax = saturations.Max(),
                OxygenSaturationMin = saturations.Min(),
                TemperatureFarenheitAverage = temperatures.Average(),
                TemperatureFarenheitMax = temperatures.Max(),
                TemperatureFarenheitMin = temperatures.Min(),
                PatientId = readings.First().Id
            };
        }
    }
}