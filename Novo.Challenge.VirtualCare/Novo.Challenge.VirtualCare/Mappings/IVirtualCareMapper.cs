namespace Novo.Challenge.VirtualCare.Mappings
{
    public interface IVirtualCareMapper
    {
        TDest Map<TDest>(object source);
    }
}