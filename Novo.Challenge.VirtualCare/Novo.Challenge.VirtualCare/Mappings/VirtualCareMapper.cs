﻿using Mapster;

namespace Novo.Challenge.VirtualCare.Mappings
{
    /// <summary>
    /// Mapper class employed to convert a source object into a desired destination object.
    /// Uses a predefined mapper definition that lists all possible conversions that are allowed in this mapper instance.
    /// </summary>
    public class VirtualCareMapper : IVirtualCareMapper
    {
        private readonly MapperDefinition m_definition;

        public VirtualCareMapper() { m_definition = new MapperDefinition(); }

        public TDest Map<TDest>(object source) => source != null ? source.Adapt<TDest>(m_definition) : default;
    }
}