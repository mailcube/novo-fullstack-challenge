using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Novo.Challenge.VirtualCare.Database;
using Novo.Challenge.VirtualCare.Events;
using Novo.Challenge.VirtualCare.Mappings;
using Novo.Challenge.VirtualCare.Services;
using Novo.Challenge.VirtualCare.Settings;

namespace Novo.Challenge.VirtualCare
{
    public class Startup
    {
        public Startup(IConfiguration configuration) { Configuration = configuration; }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Register MongoDB components
            services.Configure<VirtualCareDatabaseSettings>(
                Configuration.GetSection(nameof(VirtualCareDatabaseSettings)));
            services.AddSingleton<IPatientDbOperationManager, PatientOperationManager>();
            services.AddSingleton<IReadingOperationManager, ReadingOperationManager>();

            // Register event hub and bus
            services.AddSignalR();

            // Can be removed if unnecessary
            var msgBus = new VirtualCareEventBus();
            services.AddSingleton<IVirtualCareListener>(msgBus);
            services.AddSingleton<IVirtualCarePublisher>(msgBus);

            // Register services
            services.AddSingleton<IVirtualCareMapper, VirtualCareMapper>();
            services.AddSingleton<IPatientService, PatientService>();
            services.AddSingleton<IReadingService, ReadingService>();

            services.AddSwaggerGen();
            services.AddControllers().AddNewtonsoftJson();
            services.AddControllersWithViews();
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration => {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c => {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Virtual Care API");
                });
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => {
                endpoints.MapHub<EventHub>("eventhub");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa => {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                }
            });
        }
    }
}