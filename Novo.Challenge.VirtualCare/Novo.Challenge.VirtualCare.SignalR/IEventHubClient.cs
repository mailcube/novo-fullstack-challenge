﻿using System;
using System.Threading.Tasks;

namespace Novo.Challenge.VirtualCare.SignalR
{
    public interface IEventHubClient
    {
        Task ReadingReceived(ReadingReceivedPayload payload);
    }
}