﻿using System;

namespace Novo.Challenge.VirtualCare.SignalR
{
    public class ReadingReceivedPayload
    {
        public DateTime Timestamp { get; set; }
        public double HeartRatePerMinute { get; set; }
        public double TemperatureFarenheit { get; set; }
        public double OxygenSaturation { get; set; }
        public string PatientId { get; set; }
    }
}